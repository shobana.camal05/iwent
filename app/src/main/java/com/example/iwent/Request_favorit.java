package com.example.iwent;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class Request_favorit extends StringRequest {
    private static final String REGISTER_REQUEST_URL = "https://iwentcnam.000webhostapp.com/GetFavorites.php";
    private Map<String, String> params;

    public Request_favorit(String userId, Response.Listener<String> listener) {
        super(Request.Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("userId", userId);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
