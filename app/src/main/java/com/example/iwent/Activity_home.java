package com.example.iwent;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Activity_home extends AppCompatActivity {

    private List<String> event_id = new ArrayList<String>();
    ListView vue;
    HashMap<String, String> element = new HashMap<String, String>();
    private List<HashMap<String, String>> listeCorrecte;
    private SimpleAdapter adapter;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Intent intent = getIntent();
        if (intent != null) {
            System.out.println("------------------ on est dans l'acceuil REEL ---------------");
            String user_name = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getString("user_name", "");
            TextView nom = findViewById(R.id.textView);
            user_name = "Welcome, " + user_name + " !";
            nom.setText(user_name);

            if (intent.hasExtra("event_id")) {
                event_id = (ArrayList<String>) getIntent().getStringArrayListExtra("event_id");
                System.out.println("event_id : " + event_id);
            } else {
                //si la requête a échouée
                AlertDialog.Builder build = new AlertDialog.Builder(Activity_home.this);
                build.setMessage("Sorry\nThere was an error during loading.\nPlease try again later").setNeutralButton("Ok", null).create().show();
                finish();
            }

            //préparation des boutons
            setButtons();

            listeCorrecte = new ArrayList<HashMap<String, String>>();

            //On récupère La listeView du XML
            vue = (ListView) findViewById(R.id.listViewAccueil);
            vue.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    System.out.println("----------------- Position clické dans liste = " + position + "-------------------------");
                    ChangeActivity(view, position);
                }
            });

            TextView textView2 = findViewById(R.id.textView2);
            TextView textView3 = findViewById(R.id.textView3);


            if (event_id.isEmpty()) {
                textView2.setText("You have no events in your favorites list yet");
                textView3.setText("Find the ones you like now by launching a search !");
            } else {
                textView2.setVisibility(View.GONE);
                textView3.setVisibility(View.GONE);
                adapter = new SimpleAdapter(
                        Activity_home.this,
                        listeCorrecte,
                        R.layout.row_home,
                        new String[]{"name", "startDate", "address", "price"},
                        new int[]{R.id.name, R.id.startDate, R.id.address, R.id.PriceRange});
                vue.setAdapter(adapter);
                // préparation de la queu pour API call et lancement de la recherche
                RequestQueue queue = Volley.newRequestQueue(this);
                for(String s : event_id) {
                    System.out.println("event " + s + " is on search");
                    StringRequest stringRequest = searchNameStringRequest(s);
                    queue.add(stringRequest);
                }
            }
        } else {
            AlertDialog.Builder build = new AlertDialog.Builder(Activity_home.this);
            build.setMessage("Sorry\nThere was an error during loading.\nPlease try again later").setNeutralButton("Ok", null).create().show();
            finish();
        }
    }

    public void setButtons() {
        Button deconnexion = findViewById(R.id.Logout);
        deconnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_home.this, Activity_login.class);
                SharedPreferences pref = getSharedPreferences("PREFERENCE", MODE_PRIVATE);
                pref.edit().remove("user_id").apply();
                pref.edit().remove("user_name").apply();
                Activity_home.this.startActivity(intent);
            }
        });
        Button profil = findViewById(R.id.profile);
        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // remplacer les @@@@@@@@@ par le java de la page profil User
                Intent intent = new Intent(Activity_home.this, activity_profile.class);
                Activity_home.this.startActivity(intent);
            }
        });
        Button recherche = findViewById(R.id.Search);
        recherche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //actions a réaliser au clic
                Intent intent = new Intent(Activity_home.this, Activity_search.class);
                Activity_home.this.startActivity(intent);
            }
        });
    }

    private StringRequest searchNameStringRequest(String id) {
        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            AlertDialog.Builder build = new AlertDialog.Builder(Activity_home.this);
            build.setMessage("Sorry\nThere was an error during loading.\nPlease try again later").setNeutralButton("Ok", null).create().show();
            finish();
        }
        System.out.println("----------serachNeameStringRequest AcceuilPAssage lancé sur l'id " + id);
        //URL vers l'API
        String monUrl = "https://app.ticketmaster.com/discovery/v2/events/" + id + ".json?apikey=zLZtkKcRd3H7lXwDbadpPJGXqbZALY2F";

        Response.Listener<String> respList1;
        respList1 = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                element = new HashMap<String, String>();
                System.out.println("-------- PASSAGE DANS LE LISTENER RESPLIST 1");
                try { //parsing du Json de l'API
                    JSONObject result = new JSONObject(response);
                    String name = result.getString("name"); //nom de l'event
                    element.put("name", name);
                    String id = result.getString("id");
                    element.put("id", id);
                    System.out.println("name + id");
                    //date de début
                    String fullDate = ""; //date de début
                    if (result.has("dates")) {
                        System.out.println("dates");
                        if (result.getJSONObject("dates").has("start")) {
                            System.out.println("start");
                            JSONObject start = result.getJSONObject("dates").getJSONObject("start");
                            System.out.println("start ok");
                            if(start.has("localDate")) {
                                System.out.println("localDate is here");
                                String day = start.getString("localDate");
                                fullDate = day;
                                if (start.has("localTime")) {
                                    System.out.println("localTime is here");
                                    String hour = start.getString("localTime");
                                    fullDate += " - " + hour;
                                }
                            }
                            System.out.println("day + hour");
                            if (result.getJSONObject("dates").has("timezone")) {
                                System.out.println("has timezone");
                                String timezone = result.getJSONObject("dates").getString("timezone");
                                System.out.println("timezon OK");
                                fullDate += ", timezone : " + timezone;
                                System.out.println("fullDate is full");
                            }
                        } else { fullDate = "no given date"; System.out.println("don't have start");}
                    } else { fullDate = "No given date"; System.out.println("don't have dates");}
                    System.out.println("ended dates");
                    element.put("startDate", fullDate);
                    System.out.println("added fullDate");
                    //localisation
                    String fullPlace, longitude, latitude; //localisation
                    if(result.has("_embedded")) {
                        if (result.getJSONObject("_embedded").has("venues")) {
                            JSONObject venues = result.getJSONObject("_embedded").getJSONArray("venues").getJSONObject(0);
                            String address = venues.getJSONObject("address").getString("line1");
                            String city = venues.getJSONObject("city").getString("name");
                            String postalCode = venues.getString("postalCode");
                            String state = venues.getJSONObject("state").getString("name");
                            longitude = venues.getJSONObject("location").getString("longitude");
                            latitude = venues.getJSONObject("location").getString("latitude");
                            fullPlace = address + " " + city + " " + postalCode + " - " + state + " : long" + longitude + ", lat" + latitude;
                        } else { fullPlace = "location not specified"; longitude = "none"; latitude = "none"; }
                    } else { fullPlace = "Location not specified"; }
                    element.put("address", fullPlace);
                    System.out.println("address");
                    //prix
                    String rangePrice; //les prix
                    if (result.has("priceRanges")) {
                        String minPrice = result.getJSONArray("priceRanges").getJSONObject(0).getString("min");
                        String maxPrice = result.getJSONArray("priceRanges").getJSONObject(0).getString("max");
                        String currency = result.getJSONArray("priceRanges").getJSONObject(0).getString("currency");
                        rangePrice = minPrice + " - " + maxPrice + " " + currency;
                    } else { rangePrice = "No range price"; }
                    element.put("price", rangePrice);
                    System.out.println("price");
                    System.out.println("element ajouté : " + element);
                    System.out.println("liste avant ajout : " + listeCorrecte);
                    listeCorrecte.add(element);
                    System.out.println("liste après ajout : " + listeCorrecte);
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    AlertDialog.Builder build = new AlertDialog.Builder(Activity_home.this);
                    build.setMessage("Sorry\nThere was an error during loading.\nPlease try again later").setNeutralButton("Ok", null).create().show();
                    finish();
                }
            }
        };
        //procédure ne cas d'erreur de récupération du Json de l'API
        Response.ErrorListener respErrList = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialog.Builder build = new AlertDialog.Builder(Activity_home.this);
                build.setMessage("Sorry\nThere was an error during loading.\nPlease try again later").setNeutralButton("Ok", null).create().show();
                finish();
            }
        };
        // StringRequest( MethodType, APIurl, SuccessProc, ErrorProc)
        return new StringRequest(Request.Method.GET, monUrl,respList1, respErrList);
    }

    public void ChangeActivity(View view, int position) {
        System.out.println("listeCorrecte : " + listeCorrecte);
        final String id = listeCorrecte.get(position).get("id");
        System.out.println("event : " + listeCorrecte.get(position));
        ensureSituation(id, position);
    } //changeActivity

    public void ensureSituation(final String eventId, final int position) {
        //récupération de l'userId
        String user_id = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getString("user_id", "");
        System.out.println("Mon User_id est " + user_id);
        //requête php à la BDD
        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsResponse = new JSONObject(response);
                    boolean absent = jsResponse.getBoolean("absent");
                    String situation;
                    if(absent) {
                        situation = "absent";
                    } else {
                        situation = "present";
                    }
                    SharedPreferences pref = getSharedPreferences("PREFERENCE", MODE_PRIVATE);
                    pref.edit().putString("situation", situation).apply();
                    String sit = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getString("situation", "");
                    System.out.println("-----------------------------Situation de l'event : " + sit);
                    System.out.println("position : " + position);
                    String theSit = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getString("situation", "");
                    System.out.println("-----------------------------TheSit de l'event : " + theSit);
                    Intent intent = new Intent(Activity_home.this, Activity_event.class);
                    System.out.println("id : " + eventId);
                    intent.putExtra("id", eventId);
                    startActivity(intent);
                } catch (JSONException e) {
                    DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    };
                    AlertDialog.Builder build = new AlertDialog.Builder(Activity_home.this);
                    build.setMessage("Sorry\nThere was an error during loading.\nPlease try again later").setNegativeButton("Ok", listener).create().show();
                    finish();
                }
            }
        };
        Request_event_situation request = new Request_event_situation(eventId, user_id, listener);
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }


}
