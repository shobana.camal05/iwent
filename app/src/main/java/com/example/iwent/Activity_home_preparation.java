package com.example.iwent;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class Activity_home_preparation extends AppCompatActivity  {
    private ArrayList<String> event_id = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_preparation);
        System.out.println("on est dans l'acceuilPassage");
        String user_id = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getString("user_id", "");

        //requête php à la BDD
        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println("---------------------------Try-Catch Appel BDD acceuilPassage Lancé");
                try {
                    JSONObject jsResponse = new JSONObject(response);
                    int number = Integer.parseInt(jsResponse.getString("number"));
                    //event_id = new ArrayList<String>();
                    String value;
                    for (int i = 0; i<number; i++) {
                        value = jsResponse.getString(Integer.toString(i));
                        event_id.add(value);
                    }
                    Intent passageAccueil = new Intent(Activity_home_preparation.this, Activity_home.class);
                    System.out.println("event_id : " + event_id);
                    passageAccueil.putStringArrayListExtra("event_id", event_id);
                    Activity_home_preparation.this.startActivity(passageAccueil);
                } catch (JSONException e) {
                    AlertDialog.Builder build = new AlertDialog.Builder(Activity_home_preparation.this);
                    build.setMessage("Sorry\nThere was an error during loading.\nPlease try again later").setNeutralButton("Ok", null).create().show();
                    finish();
                }
                System.out.println("----------------------------Try-Catch Appel BDD AcceuilPAssage Terminé");
            }
        };
        Request_favorit favRequest = new Request_favorit(user_id, listener);
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(favRequest);
    }
}
