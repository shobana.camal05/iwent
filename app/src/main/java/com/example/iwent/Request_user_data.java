package com.example.iwent;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

class Request_user_data extends StringRequest {

    private static final String GETUSERDATA_REQUEST_URL = "https://iwentcnam.000webhostapp.com/GetUser.php";
    private Map<String, String> params;

    public Request_user_data(String userId, Response.Listener<String> listener){
        super(Method.POST, GETUSERDATA_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("userId", userId);

    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
