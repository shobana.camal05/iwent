package com.example.iwent;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.iwent.ui.Request_event_add;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import org.json.JSONException;
import org.json.JSONObject;

public class Activity_event extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private static final int TAG_CODE_PERMISSION_LOCATION = 123;
    private String eventId = "";
    private Button register;
    private String situation;
    SupportMapFragment mapFragment;
    GoogleMap map;
    String name, latitude, longitude;
    Activity_event me = this;

    private void setSituation(String nouveau) {
        situation = nouveau;
    }

    private StringRequest searchNameStringRequest(String id) {
        //URL vers l'API
        String monUrl = "https://app.ticketmaster.com/discovery/v2/events/" + id + ".json?apikey=zLZtkKcRd3H7lXwDbadpPJGXqbZALY2F";
        System.out.println("------ DONNEE COLLECTEES -----");

        Response.Listener<String> respList1;
        respList1 = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println("-------- PASSAGE DANS LE LISTENER RESPLIST 1");
                try { //parsing du Json de l'API
                    JSONObject result = new JSONObject(response);
                    name = result.getString("name"); //nom de l'event
                    String url = result.getString("url"); // url vers la page officielle
                    String rangePrice; //les prix
                    if (result.has("priceRanges")) {
                        String minPrice = result.getJSONArray("priceRanges").getJSONObject(0).getString("min");
                        String maxPrice = result.getJSONArray("priceRanges").getJSONObject(0).getString("max");
                        String currency = result.getJSONArray("priceRanges").getJSONObject(0).getString("currency");
                        rangePrice = minPrice + " - " + maxPrice + " " + currency;
                    } else { rangePrice = "No range price"; }
                    String fullDate = ""; //date de début
                    if (result.has("dates")) {
                        System.out.println("----------- event hash dates --------");
                        if (result.getJSONObject("dates").has("start")) {
                            System.out.println("------------- event has start dates ----------");
                            JSONObject start = result.getJSONObject("dates").getJSONObject("start");
                            if (start.has("localDate")) {
                                String day = start.getString("localDate");
                                fullDate = day;
                                if (start.has("localTime")) {
                                    String hour = start.getString("localTime");
                                    fullDate += " - " + hour;
                                }
                            }
                            if (result.getJSONObject("dates").has("timezone")) {
                                String timezone = result.getJSONObject("dates").getString("timezone");
                                System.out.println("timezon OK");
                                fullDate += ", timezone : " + timezone;
                            }
                        } else { fullDate = "no given date"; }
                    } else { fullDate = "No given date"; }
                    String fullPlace; //localisation
                    if(result.has("_embedded")) {
                        if (result.getJSONObject("_embedded").has("venues")) {
                            JSONObject venues = result.getJSONObject("_embedded").getJSONArray("venues").getJSONObject(0);
                            String address = venues.getJSONObject("address").getString("line1");
                            String city = venues.getJSONObject("city").getString("name");
                            String postalCode = venues.getString("postalCode");
                            String state = venues.getJSONObject("state").getString("name");
                            longitude = venues.getJSONObject("location").getString("longitude");
                            latitude = venues.getJSONObject("location").getString("latitude");
                            fullPlace = address + " " + city + " " + postalCode + " - " + state + " : long" + longitude + ", lat" + latitude;
                        } else { fullPlace = "location not specified"; longitude = "none"; latitude = "none"; }
                    } else { fullPlace = "Location not specified"; }
                    String limit; //nombre de places restantes
                    if(result.has("accessibility")) {
                        if (result.getJSONObject("accessibility").has("ticketLimit")) {
                            limit = "Only " + result.getJSONObject("accessibility").getString("ticketLimit") + " tickets left";
                        } else { limit = "no limit"; }
                    } else { limit = "No Ticket limit"; }
                    String segment, genre, subGenre, type, subType; //themes
                    if(result.has("classifications")) {
                        JSONObject classif = result.getJSONArray("classifications").getJSONObject(0);
                        if(classif.has("segment")) { segment = "segment : " + classif.getJSONObject("segment").getString("name");
                        } else { segment = "No segment specified"; }
                        if(classif.has("genre")) { genre = "genre : " + classif.getJSONObject("genre").getString("name");
                        } else { genre = ""; }
                        if(classif.has("subGenre")) {
                            if(genre.equals("")) {
                                subGenre = " genre : " + classif.getJSONObject("subGenre").getString("name");
                            } else {
                                subGenre = " - " + classif.getJSONObject("subGenre").getString("name");
                            }
                        } else { subGenre = ""; }
                        if(genre.equals("") && subGenre.equals("")) {
                            genre = "No genre given";
                        }
                        if(classif.has("type")) { type = "type : " + classif.getJSONObject("type").getString("name");
                        } else { type = ""; }
                        if(classif.has("subType")) {
                            if(type.equals("")) {
                                subType = " Type : " + classif.getJSONObject("subType").getString("name");
                            } else {
                                subType = " - " + classif.getJSONObject("subType").getString("name");
                            }
                        } else { subType = ""; }
                        if(type.equals("") && subType.equals("")){
                            type = "No type specified";
                        }
                    } else { segment = "No segement specified"; genre = "No genre given"; subGenre = ""; type = "No type either"; subType = ""; }
                    String legalAge; //age légal requis
                    if(result.has("ageRestrictions")) {
                        legalAge = result.getJSONObject("ageRestrictions").getString("legalAgeEnforced");
                        if (legalAge.equals("false")) {
                            legalAge = "No age restriction";
                        } else {
                            legalAge = "This event is only for 18+";
                        }
                    } else { legalAge = "No age restriction"; }
                    TextView nom = findViewById(R.id.textView4);
                    nom.setText(name);
                    TextView prix = findViewById(R.id.textView3);
                    prix.setText(rangePrice);
                    //utiliser longitude et latitude pour afficher la position de l'event sur la carte
                    TextView dates = findViewById(R.id.textViewFullDate);
                    dates.setText(fullDate);
                    TextView adresse = findViewById(R.id.textViewFullPlace);
                    adresse.setText(fullPlace);
                    TextView leSegment = findViewById(R.id.textViewSegment);
                    leSegment.setText(segment);
                    TextView leGenre = findViewById(R.id.textViewGenre);
                    leGenre.setText(genre);
                    TextView leSubGenre = findViewById(R.id.textViewSubGenre);
                    leSubGenre.setText(subGenre);
                    TextView leType = findViewById(R.id.textViewType);
                    leType.setText(type);
                    TextView leSubType = findViewById(R.id.textViewSubType);
                    leSubType.setText(subType);
                    TextView laLimite = findViewById(R.id.textViewLimit);
                    laLimite.setText(limit);
                    TextView ageLegal = findViewById(R.id.textViewLegalAge);
                    ageLegal.setText(legalAge);
                    TextView siteOfficiel = findViewById(R.id.textViewUrl);
                    siteOfficiel.setText(url);
                    //map
                    mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
                    mapFragment.getMapAsync(me);
                } catch (JSONException e) {
                    //si la requête a échouée
                    AlertDialog.Builder build = new AlertDialog.Builder(Activity_event.this);
                    build.setMessage("Sorry\nThere was an error during loading.\nPlease try again later").setNeutralButton("Ok", null).create().show();
                    finish();
                }
            }
        };

        //procédure ne cas d'erreur de récupération du Json de l'API
        Response.ErrorListener respErrList = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //si la requête a échouée
                AlertDialog.Builder build = new AlertDialog.Builder(Activity_event.this);
                build.setMessage("Sorry\nThere was an error during loading.\nPlease try again later").setNeutralButton("Ok", null).create().show();
                finish();
            }
        };
        // StringRequest( MethodType, APIurl, SuccessProc, ErrorProc)
        return new StringRequest(Request.Method.GET, monUrl,respList1, respErrList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);
        Intent intent = getIntent();

        if (intent!=null) {
            if (intent.hasExtra("id")) { eventId = intent.getStringExtra("id"); }
            situation = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getString("situation", "");
            System.out.println("-----------------------------Situation de l'event : " + situation);
            register = findViewById(R.id.achatButton);
            if (situation.equals("absent")) {
                register.setText("Add to Favorites");
            } else {
                register.setText("remove from Favorites");
            }
            register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    registerAction(eventId, situation);
                }
            });

            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            TextView toolbarTitle = (TextView) findViewById(R.id.toolbarTitle);
            toolbarTitle.setText("Go home");
            LinearLayout layout = (LinearLayout) findViewById(R.id.LinearToolBar);
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("ToolBar Clicked");
                    Intent goHome = new Intent(Activity_event.this, Activity_home_preparation.class);
                    Activity_event.this.startActivity(goHome);
                }
            });

            Button pagePrecedente = findViewById(R.id.retour);
            pagePrecedente.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            // préparation de la queu pour API call et lancement de la recherche
            RequestQueue queue = Volley.newRequestQueue(this);
            System.out.println("event i : " + eventId);
            StringRequest stringRequest = searchNameStringRequest(eventId);
            queue.add(stringRequest);
        } else {
            //si la requête a échouée
            AlertDialog.Builder build = new AlertDialog.Builder(Activity_event.this);
            build.setMessage("Sorry\nThere was an error during loading.\nPlease try again later").setNeutralButton("Ok", null).create().show();
            finish();}
    }

    public void registerAction(String eventId, String situation) {
        //récupération de l'userId
        String user_id = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getString("user_id", "");
        System.out.println("Mon User_id est toujours " + user_id);
        //requête php à la BDD
        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    System.out.println("inside the try");
                    System.out.println(response);
                    JSONObject jsResponse = new JSONObject(response);
                    boolean success = jsResponse.getBoolean("success");
                    if(success) {
                        boolean added = jsResponse.getBoolean("added");
                        if(added) { //event ajouté a la liste de favoris
                            AlertDialog.Builder build = new AlertDialog.Builder(Activity_event.this);
                            build.setMessage("This event is now on your favorites list.\nOur application does not manage the purchase of tickets.\nIf necessary, please register for the event on its official website to participate.").setNeutralButton("Ok", null).create().show();
                            register.setText("remove from Favorites");
                            setSituation("present");
                        } else { // event retire de la liste des favorits
                            AlertDialog.Builder build = new AlertDialog.Builder(Activity_event.this);
                            build.setMessage("This event is no longer in your favorites list.\nIf you have purchased tickets, please refer to the official website to cancel them.").setNegativeButton("Ok", null).create().show();
                            register.setText("Add to Favorites");
                            setSituation("absent");
                        }
                    } else {
                        //si la requête a échouée
                        AlertDialog.Builder build = new AlertDialog.Builder(Activity_event.this);
                        build.setMessage("Sorry\nThere was an error during loading.\nPlease try again later").setNeutralButton("Ok", null).create().show();
                    }
                } catch (JSONException e) {
                    //si la requête a échouée
                    AlertDialog.Builder build = new AlertDialog.Builder(Activity_event.this);
                    build.setMessage("Sorry\nThere was an error during loading.\nPlease try again later").setNeutralButton("Ok", null).create().show();
                }
            }
        };

        Request_event_add eventAddRequest = new Request_event_add(eventId, user_id, situation, listener);
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(eventAddRequest);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        LatLng coordEvent;
        map.setOnMarkerClickListener(this);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        } else {
            ActivityCompat.requestPermissions(this, new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION },
                    TAG_CODE_PERMISSION_LOCATION);
        }

        boolean premierElement = true;
        System.out.println(" NAME /////////// "+ name);
        System.out.println(" LONG /////////// "+ longitude);
        System.out.println(" LAT /////////// "+ latitude);
        System.out.println("---------");

        coordEvent = new LatLng(Double.parseDouble(latitude),Double.parseDouble(longitude));
        googleMap.addMarker(new MarkerOptions().position(coordEvent).title(name));

        if(premierElement)
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(coordEvent));
        premierElement=false;

    }
}
