package com.example.iwent;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

public class Activity_search extends AppCompatActivity {

    @SuppressLint("RestrictedApi")
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView toolbarTitle = (TextView) findViewById(R.id.toolbarTitle);
        toolbarTitle.setText("Go home");
        LinearLayout layout = (LinearLayout) findViewById(R.id.LinearToolBar);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("ToolBar Clicked");
                Intent goHome = new Intent(Activity_search.this, Activity_home_preparation.class);
                Activity_search.this.startActivity(goHome);
            }
        });

        final Button searchButton = (Button) findViewById(R.id.okButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("---------> JAI CLIQUE SUR LE BOUTON -----------------");
                ChangeActivity(v);
            }
        });
    }

    public void ChangeActivity(View view) {
        //on récupère les infos du xml par leurs ID
        TextView eRechercheBar = (TextView) findViewById(R.id.RechercheBar);
        TextView eOuRechercheBar = (TextView) findViewById(R.id.ouRechercheBar);
        EditText eDateDeb = (EditText) findViewById(R.id.dateDeb);
        EditText eDateFin = (EditText) findViewById(R.id.dateFin);
        CheckBox eCheckBox = (CheckBox) findViewById(R.id.checkBox);
        CheckBox eCheckBox2 = (CheckBox) findViewById(R.id.checkBox2);
        CheckBox eCheckBox3 = (CheckBox) findViewById(R.id.checkBox3);
        Switch eSwitch1 = (Switch) findViewById(R.id.switch1);
        //on récupère les contenus de ces infos
        String rechercheBar = eRechercheBar.getText().toString();
        String ouRechercheBar = eOuRechercheBar.getText().toString();
        String dateDeb = eDateDeb.getText().toString();
        String dateFin = eDateFin.getText().toString();
        Boolean checkBox = eCheckBox.isChecked();
        Boolean checkBox2 = eCheckBox2.isChecked();
        Boolean checkBox3 = eCheckBox3.isChecked();
        Boolean switch1 = eSwitch1.isChecked();
        //on prépare le message à envoyer : création + remplissage + envoi de l'intent
        Intent intent = new Intent(this, Activity_result_list.class);
        intent.putExtra("rechercheBar",rechercheBar);
        intent.putExtra("ouRechercheBar",ouRechercheBar);
        intent.putExtra("dateDeb", dateDeb);
        intent.putExtra("dateFin",dateFin);
        intent.putExtra("checkBox",checkBox);
        intent.putExtra("checkBox2",checkBox2);
        intent.putExtra("checkBox3",checkBox3);
        intent.putExtra("switch1",switch1);
        startActivity(intent);
    }

}
