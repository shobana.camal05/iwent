package com.example.iwent;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class activity_profile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        final TextView nom = findViewById(R.id.nom);
        final TextView mail = findViewById(R.id.mail);
        final TextView mdp = findViewById(R.id.mdp);
        final Button edit = (Button) findViewById(R.id.editdata);
        final String user_id = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getString("user_id", "");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView toolbarTitle = (TextView) findViewById(R.id.toolbarTitle);
        toolbarTitle.setText("Go home");
        LinearLayout layout = (LinearLayout) findViewById(R.id.LinearToolBar);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("ToolBar Clicked");
                Intent goHome = new Intent(activity_profile.this, Activity_home_preparation.class);
                activity_profile.this.startActivity(goHome);
            }
        });

        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonResponse = new JSONObject(response);
                    jsonResponse = jsonResponse.getJSONObject("data");
                    nom.setText(jsonResponse.getString("username"));
                    mail.setText(jsonResponse.getString("mail"));
                    mdp.setText("***********");

                } catch (JSONException e) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(activity_profile.this);
                    builder.setMessage("Login Failed").setNegativeButton("Retry", null).create().show();
                    finish();
                }
            }
        };
        Request_user_data userdatarequest = new Request_user_data(user_id, responseListener);
        RequestQueue queue = Volley.newRequestQueue(activity_profile.this);
        queue.add(userdatarequest);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity_profile.this, Activity_profile_change.class);
                intent.putExtra("username", nom.getText().toString());
                intent.putExtra("mail", mail.getText().toString());
                startActivity(intent);
            }
        });
    }
}
