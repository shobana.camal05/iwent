package com.example.iwent;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

class Request_edit_user extends StringRequest {

    private static final String EDITUSER_REQUEST_URL = "https://iwentcnam.000webhostapp.com/EditUser.php";
    private Map<String, String> params;

    public Request_edit_user(String userId, String username, String email, String psw, Response.Listener<String> listener) {
        super(Method.POST, EDITUSER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("id", userId);
        params.put("username", username);
        params.put("mail", email);
        params.put("password", psw);

    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
