package com.example.iwent;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class Activity_login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText etMail = (EditText) findViewById(R.id.Mailuser);
        final EditText etPassword = (EditText) findViewById(R.id.mdp);
        final Button bLogin = (Button) findViewById(R.id.connectButton);
        final Button bRegister = (Button) findViewById(R.id.registerButton);

        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String mail = etMail.getText().toString();
                final String password = etPassword.getText().toString();
                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonResponse = new JSONObject(response);
                            boolean success = jsonResponse.getBoolean("success");

                            if(success){
                                //intent pour transmettre des infos a l'activité suivante
                                String username = jsonResponse.getString("username");
                                String id = jsonResponse.getString("user_id");
                                Intent intent = new Intent(Activity_login.this, Activity_home_preparation.class);
                                //shared Preferences pour garder en tête le nom de l'user
                                SharedPreferences pref = getSharedPreferences("PREFERENCE", MODE_PRIVATE);
                                pref.edit().putString("user_id", id).apply();
                                pref.edit().putString("user_name", username).apply();
                                Activity_login.this.startActivity(intent);
                            }else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(Activity_login.this);
                                builder.setMessage("Login Failed").setNegativeButton("Retry", null).create().show();
                            }
                        } catch (JSONException e) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(Activity_login.this);
                            builder.setMessage("Login Failed").setNegativeButton("Retry", null).create().show();
                        }
                    }
                };

                Request_login loginRequest = new Request_login(mail, password, responseListener);
                RequestQueue queue = Volley.newRequestQueue(Activity_login.this);
                queue.add(loginRequest);
            }
        });

        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registerIntent = new Intent(Activity_login.this, Activity_Register.class);
                Activity_login.this.startActivity(registerIntent);
            }
        });
    }
}
