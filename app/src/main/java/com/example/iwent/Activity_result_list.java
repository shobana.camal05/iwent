package com.example.iwent;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Activity_result_list extends AppCompatActivity {

    //création de la vue sur la liste à remplir dans la page activity_result_list.wml
    ListView vue;
    //création de la liste des éléments à afficher
    private List<HashMap<String, String>> elementsList;
    Activity_result_list me = this;

    //méthode de création de la StringRequest
    private StringRequest searchNameStringRequest(String motCle, String dateDebut, String dateFin, String location, Boolean bool, Boolean chk1, Boolean chk2, Boolean chk3, Boolean chk4, Boolean chk5, Boolean chk6) {
        // 2eme param de la fonction StringRequest en dessous : création de l'url complet pour l'API call
        final String API = "&apikey=zLZtkKcRd3H7lXwDbadpPJGXqbZALY2F"; //API KEY (récupérée sur le site de l'API)
        String sort = "sort=date,asc";
        String CountryCode = "&countryCode=US";    //code du pays ou effectuer les recherches, à remplacer par celui de la france
        String Keyword, city, StartDate, EndDate, latlong, genre, type;
        Keyword = "&keyword=" + motCle; //mot clé pour la recherche
        if (location.equals("")) { city = "";} else { city = "&city=" + location; CountryCode = ""; }// city
        StartDate = "&startDateTime=" + dateDebut;
        EndDate = "&endDateTime=" + dateFin;
        if (bool) {
            LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                System.out.println("error with long lat");
                latlong = "";
            } else {
                Location theLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                double longitude = theLocation.getLongitude();
                double latitude = theLocation.getLatitude();
                System.out.println("longitude : " + longitude + "\nlatitude : " + latitude);
                latlong = "&latlong=" + latitude + "," + longitude;
                CountryCode = "";
            }
        } else { latlong = ""; };
        if (chk1) { genre = "&segmentId=KZFzniwnSyZfZ7v7nJ"; } else if (chk2) { genre = "&segmentId=KZFzniwnSyZfZ7v7nE"; } else if (chk3) { genre = "&segmentId=KZFzniwnSyZfZ7v7na"; } else { genre = "";}
        if (chk4) { type = "&subTypeId=KZFzBErXgnZfZ7vA6E"; } else if (chk5) { type = "&subTypeId=KZFzBErXgnZfZ7vAkJ"; } else if (chk6) { type = "&subGenreId=KZazBEonSMnZfZ7v7n1"; } else { type = "";}
        // pour modifier l'ordre de tri : &sort=relevance,descs => name,date,asc / name,date,asc / date,name,asc / date,name,desc / distance,date,asc
        final String URL_PREFIX = "https://app.ticketmaster.com/discovery/v2/events.json?";
        String monUrl = URL_PREFIX + sort + CountryCode + city + Keyword + StartDate + EndDate + latlong + genre + type + API;
        System.out.println("Mon url est : " + monUrl);

        //3eme param de la fonction StringRequest en dessous : procédure à suivre en cas de succès
        Response.Listener<String> respList1 = new Response.Listener<String>() {
            //System.out.println("-------- PASSAGE DANS LE Listener respList1 ---------");
            @Override
            public void onResponse(String response) {
                // try/catch block for returned JSON data, see API's documentation for returned format
                try {
                    JSONObject result = new JSONObject(response);
                    //System.out.println(result.toString(5));
                    System.out.println(result.toString());
                    if (result.has("_embedded")) {
                        //HashMap <clé, valeur> : name, id, address, city, postalCode, min, max, currency, startDate, startTime
                        elementsList = new ArrayList<>();
                        HashMap<String, String> element;

                        //début du parsing
                        JSONArray array = result.getJSONObject("_embedded").getJSONArray("events");
                        //pour chaque evenement trouvé dans le json
                        for (int i = 0; i < array.length(); i++) {
                            //on crée un élément pour la listes
                            element = new HashMap<>();
                            //on récupère les informations de l'evenement et on les ajoute à l'élément
                            // NAME + ID de l'EVENT
                            element.put("name", array.getJSONObject(i).getString("name"));
                            element.put("id", array.getJSONObject(i).getString("id"));

                            // LOCALISATION DE L'EVENT
                            if (array.getJSONObject(i).has("_embedded")) {
                               if (array.getJSONObject(i).getJSONObject("_embedded").has("venues")) {
                                    JSONArray venues = array.getJSONObject(i).getJSONObject("_embedded").getJSONArray("venues");
                                    String address = venues.getJSONObject(0).getJSONObject("address").getString("line1");
                                    if(venues.getJSONObject(0).has("city")) {
                                        address += " - " + venues.getJSONObject(0).getJSONObject("city").getString("name");
                                    }
                                    if(venues.getJSONObject(0).has("postalCode")) {
                                        address += " - " + venues.getJSONObject(0).getString("postalCode");
                                    }
                                    element.put("address", address);
                                    if (venues.getJSONObject(0).has("location")) {
                                        element.put("longitude", venues.getJSONObject(0).getJSONObject("location").getString("longitude"));
                                        element.put("latitude", venues.getJSONObject(0).getJSONObject("location").getString("latitude"));
                                        System.out.println("----- " + venues.getJSONObject(0).getJSONObject("location").getString("latitude"));
                                    }
                                }
                            }

                            // DATE ET HEURE DE L'EVENT
                            if (array.getJSONObject(i).has("dates")) {
                                JSONObject start = array.getJSONObject(i).getJSONObject("dates").getJSONObject("start");
                                String startDate = start.getString("localDate");
                                if (start.has("localTime")) {
                                    startDate += " - " + start.getString("localTime");
                                }
                                element.put("startDate", startDate);
                            }
                            //on ajoute l'élément créé à  la liste
                            elementsList.add(element);
                            System.out.println("-------- event ajouté à la liste --------");
                        }

                        System.out.println("-------- affichage de la liste des evenements --------");
                        // On crée l'adapter et on l'ajoute à la vue pour affichage : ListeAdapter adapter = new SimpleAdapte( this, ValeursAInserer, layoutDeChaqueElement, cles, layoutDeChaqueWidget)
                        ListAdapter adapter = new SimpleAdapter(
                                me,
                                elementsList,
                                //android.R.layout.simple_list_item_2,
                                R.layout.row_result,
                                new String[]{"name", "startDate", "address"},
                                new int[]{R.id.name, R.id.startDate, R.id.address});
                        vue.setAdapter(adapter);
                    } else {
                        //si la requête n'échoue pas mais qu'elle ne retourne aucun résultat
                        AlertDialog.Builder build = new AlertDialog.Builder(Activity_result_list.this);
                        build.setMessage("Sorry\nThere isn't any event corresponding to your search.\nPlease try with simpler informations.").
                                setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).create().show();
                    }
                } catch (JSONException e) {
                    //si la requête a échouée
                    DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    };
                    AlertDialog.Builder build = new AlertDialog.Builder(Activity_result_list.this);
                    build.setMessage("Sorry\nThere was an error during loading.\nPlease try again later").setNegativeButton("Ok", listener).create().show();
                }
            }
        };

        //4eme param de la fonction StringRequest en dessous : procédure à suivre en cas d'erreur
        Response.ErrorListener respErrList = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //si la requête a échouée
                AlertDialog.Builder build = new AlertDialog.Builder(Activity_result_list.this);
                build.setMessage("Sorry\nThere was an error during loading.\nPlease try again later").setNeutralButton("Ok", null).create().show();
                Intent toResearch = new Intent(Activity_result_list.this, Activity_home_preparation.class);
                Activity_result_list.this.startActivity(toResearch);
            }
        };

        // StringRequest( MethodType, APIurl, SuccessProc, ErrorProc)
        return new StringRequest(Request.Method.GET, monUrl,respList1, respErrList);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_list);

        Intent intent = getIntent();
        if (intent != null) {   // il est sensé ne jamais être nul, mais permet d’éviter de crashs

            //toolbar
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            TextView toolbarTitle = (TextView) findViewById(R.id.toolbarTitle);
            toolbarTitle.setText("Go home");
            LinearLayout layout = (LinearLayout) findViewById(R.id.LinearToolBar);
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    System.out.println("ToolBar Clicked");
                    Intent goHome = new Intent(Activity_result_list.this, Activity_home_preparation.class);
                    Activity_result_list.this.startActivity(goHome);
                }
            });
            //button for go to map
            final Button gotomap = (Button) findViewById(R.id.gotomap);
            gotomap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent mapIntent = new Intent(Activity_result_list.this, Activity_result_map.class);
                    mapIntent.putExtra("elementsList", (ArrayList<HashMap<String,String>>) elementsList);
                    Activity_result_list.this.startActivity(mapIntent);
                }
            });
            //Boutons sur la ListView pour chaque event qui sera affiché
            vue = (ListView) findViewById(R.id.listView);
            vue.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    System.out.println("----------------- Position clické dans liste = " + position + "-------------------------");
                    ChangeActivity(position);
                }
            });

            //on récupère les informations de la page parent
            String rechercheBar = ""; if (intent.hasExtra("rechercheBar")) { rechercheBar = intent.getStringExtra("rechercheBar"); }
            String ouRechercheBar = ""; if (intent.hasExtra("ouRechercheBar")) { ouRechercheBar = intent.getStringExtra("ouRechercheBar"); }
            if (!ouRechercheBar.equals("")) {
                ouRechercheBar = ouRechercheBar.replace(" ", "+");
            }
            String dateDeb = ""; if (intent.hasExtra("dateDeb")) { dateDeb = intent.getStringExtra("dateDeb"); }
            if (!dateDeb.equals("")) {
                dateDeb = dateDeb.substring(6, 10) + "-" + dateDeb.substring(3, 5) + "-" + dateDeb.substring(0, 2) + "T00:00:00Z";
            }
            String dateFin = ""; if (intent.hasExtra("dateFin")) { dateFin = intent.getStringExtra("dateFin"); }
            if (!dateFin.equals("")) {
                dateFin = dateFin.substring(6, 10) + "-" + dateFin.substring(3, 5) + "-" + dateFin.substring(0, 2) + "T00:00:00Z";
            }
            Boolean checkBox = intent.getBooleanExtra("checkBox", false);
            Boolean checkBox2 = intent.getBooleanExtra("checkBox2", false);
            Boolean checkBox3 = intent.getBooleanExtra("checkBox3", false);
            Boolean checkBox4 = intent.getBooleanExtra("checkBox3", false);
            Boolean checkBox5 = intent.getBooleanExtra("checkBox3", false);
            Boolean checkBox6 = intent.getBooleanExtra("checkBox3", false);
            Boolean switch1 = intent.getBooleanExtra("switch1", false);
            System.out.println("-------- INFOS DE RECHERCHE RECUPERES --------");

            // préparation de la queu pour API call et lancement de la recherche
            //création de la queue pour les API calls
            RequestQueue queue = Volley.newRequestQueue(this);
            StringRequest stringRequest = searchNameStringRequest(rechercheBar, dateDeb, dateFin, ouRechercheBar, switch1, checkBox, checkBox2, checkBox3, checkBox4, checkBox5, checkBox6); //Search(motCle, dateDebut, dateFin)
            queue.add(stringRequest);
        } else {
            //ajouter une Pop-up qui dit erreur et renvoie à la page de recherche?
            AlertDialog.Builder builder = new AlertDialog.Builder(Activity_result_list.this);
            builder.setMessage("Register Failed\nEmail already used").setNegativeButton("Retry", null).create().show();
        }
    } //OnCreate

    public void ChangeActivity(int position) {
        final String id = elementsList.get(position).get("id");
        ensureSituation(id, position);
    } //changeActivity

    public void ensureSituation(final String eventId, final int position) {
        //récupération de l'userId
        String user_id = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getString("user_id", "");
        System.out.println("Mon User_id est " + user_id);
        //requête php à la BDD
        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsResponse = new JSONObject(response);
                    boolean absent = jsResponse.getBoolean("absent");
                    String situation;
                    if(absent) {
                        situation = "absent";
                    } else {
                        situation = "present";
                    }
                    SharedPreferences pref = getSharedPreferences("PREFERENCE", MODE_PRIVATE);
                    pref.edit().putString("situation", situation).apply();
                    String sit = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getString("situation", "");
                    System.out.println("-----------------------------Situation de l'event : " + sit);
                    System.out.println("position : " + position);
                    String theSit = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getString("situation", "");
                    System.out.println("-----------------------------TheSit de l'event : " + theSit);
                    Intent intent = new Intent(me, Activity_event.class);
                    System.out.println("id : " + eventId);
                    intent.putExtra("id", eventId);
                    startActivity(intent);
                } catch (JSONException e) {
                    //si la requête a échouée
                    DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    };
                    AlertDialog.Builder build = new AlertDialog.Builder(Activity_result_list.this);
                    build.setMessage("Sorry\nThere was an error during loading.\nPlease try again later").setNegativeButton("Ok", listener).create().show();
                    finish();
                }
            }
        };
        Request_event_situation request = new Request_event_situation(eventId, user_id, listener);
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }
} // class
