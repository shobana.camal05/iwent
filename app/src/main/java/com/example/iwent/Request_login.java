package com.example.iwent;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class Request_login extends StringRequest {
    private static final String LOGIN_REQUEST_URL = "https://iwentcnam.000webhostapp.com/Login.php";
    private Map<String, String> params;

    public Request_login(String mail, String password, Response.Listener<String> listener){
        super(Request.Method.POST, LOGIN_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("mail", mail);
        params.put("password", password);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
