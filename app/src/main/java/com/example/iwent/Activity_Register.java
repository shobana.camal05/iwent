package com.example.iwent;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

public class Activity_Register extends AppCompatActivity {

    public static boolean isValid(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z" + "A-Z]{2,7}$";
        Pattern pat = Pattern.compile(emailRegex);
        if (email == null) { return false; } else { return pat.matcher(email).matches(); }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        final EditText etUsername = (EditText) findViewById(R.id.InscipUserName);
        final EditText etMail = (EditText) findViewById(R.id.inscipMail);
        final EditText etPassword = (EditText) findViewById(R.id.inscipMdp);
        final EditText etConfirmMdp = (EditText) findViewById(R.id.ConfirMdp);
        final Button bRegister = (Button) findViewById(R.id.InscrtButton);
        final Button bLoging = (Button) findViewById(R.id.LogingButton);

        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username = etUsername.getText().toString();
                final String mail = etMail.getText().toString();
                final String password = etPassword.getText().toString();
                final String confirmPassword = etConfirmMdp.getText().toString();

                //tous les champs sont requis
                if (username.equals("") || mail.equals("") || password.equals("") || confirmPassword.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Activity_Register.this);
                    builder.setMessage("Register Failed\nPlease fill all inputs").setNegativeButton("Retry", null).create().show();
                }
                //le mail doit être valide
                else if (!isValid(mail)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Activity_Register.this);
                    builder.setMessage("Register Failed\nPlease give a valid mail").setNegativeButton("Retry", null).create().show();
                }
                //password doit etre identique à confirmPassword
                else if (!password.equals(confirmPassword)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Activity_Register.this);
                    builder.setMessage("Register Failed\nConfirmation different from Password").setNegativeButton("Retry", null).create().show();
                }
                else {
                    Response.Listener<String> responseListener = new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                boolean success = jsonResponse.getBoolean("success");
                                if (success) {
                                    Intent intent = new Intent(Activity_Register.this, Activity_login.class);
                                    Activity_Register.this.startActivity(intent);
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(Activity_Register.this);
                                    builder.setMessage("Register Failed\nEmail already used").setNegativeButton("Retry", null).create().show();
                                }
                            } catch (JSONException e) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(Activity_Register.this);
                                builder.setMessage("Register Failed\nPlease retry").setNegativeButton("Retry", null).create().show();
                            }
                        }
                    };
                    Request_register registerRequest = new Request_register(username, mail, password, responseListener);
                    RequestQueue queue = Volley.newRequestQueue(Activity_Register.this);
                    queue.add(registerRequest);
                }
            }
        });

        bLoging.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent logingIntent = new Intent(Activity_Register.this, Activity_login.class);
                Activity_Register.this.startActivity(logingIntent);
            }
        });
    }
}
