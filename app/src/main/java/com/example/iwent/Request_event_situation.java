package com.example.iwent;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class Request_event_situation extends StringRequest {
    private static final String REGISTER_REQUEST_URL = "https://iwentcnam.000webhostapp.com/GetEvent.php";
    private Map<String, String> params;

    public Request_event_situation(String eventId, String userId, Response.Listener<String> listener) {
        super(Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("eventId", eventId);
        params.put("userId", userId);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}