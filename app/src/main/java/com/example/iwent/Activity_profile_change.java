package com.example.iwent;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

public class Activity_profile_change extends AppCompatActivity {

    public static boolean isValid(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+ "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z" + "A-Z]{2,7}$";
        Pattern pat = Pattern.compile(emailRegex);
        if (email == null) { return false; } else { return pat.matcher(email).matches(); }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_change);
        final Button goback = (Button) findViewById(R.id.goback);
        final EditText nom = (EditText) findViewById(R.id.nomchange);
        final EditText mail = (EditText) findViewById(R.id.mailchange);
        final EditText mdp = (EditText) findViewById(R.id.mdpchange);
        final Button ok = (Button) findViewById(R.id.ok);
        final String user_id = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getString("user_id", "");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView toolbarTitle = (TextView) findViewById(R.id.toolbarTitle);
        toolbarTitle.setText("Go home");
        LinearLayout layout = (LinearLayout) findViewById(R.id.LinearToolBar);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("ToolBar Clicked");
                Intent goHome = new Intent(Activity_profile_change.this, Activity_home_preparation.class);
                Activity_profile_change.this.startActivity(goHome);
            }
        });

        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Activity_profile_change.this, activity_profile.class);
                startActivity(intent);
            }
        });

        Intent intent = getIntent();
        String username = intent.getStringExtra("username");
        final String email = intent.getStringExtra("mail");
        nom.setText(username);
        mail.setText(email);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Response.Listener<String> responseListener = new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        final String username = nom.getText().toString();
                        final String adrrMail = mail.getText().toString();

                        //le mail doit être valide
                        if (adrrMail.equals("")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(Activity_profile_change.this);
                            builder.setMessage("Register Failed\nPlease fill mail").setNegativeButton("Retry", null).create().show();
                        } else if (username.equals("")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(Activity_profile_change.this);
                            builder.setMessage("Register Failed\nPlease fill username").setNegativeButton("Retry", null).create().show();
                        } else if (!isValid(adrrMail)) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(Activity_profile_change.this);
                            builder.setMessage("Register Failed\nPlease give a valid mail").setNegativeButton("Retry", null).create().show();
                        } else {
                            try {
                                System.out.println(response);
                                JSONObject jsonResponse = new JSONObject(response);
                                boolean success = jsonResponse.getBoolean("success");
                                if (success) {
                                    Intent intent = new Intent(Activity_profile_change.this, activity_profile.class);
                                    startActivity(intent);
                                } else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(Activity_profile_change.this);
                                    builder.setMessage("Register Failed\nEmail already used")
                                            .setNegativeButton("Retry", null)
                                            .create()
                                            .show();
                                }
                            } catch (JSONException e) {
                                System.out.println("ToolBar Clicked");
                                Intent goHome = new Intent(Activity_profile_change.this, Activity_home_preparation.class);
                                Activity_profile_change.this.startActivity(goHome);
                            }
                        }
                    }
                };
                Request_edit_user edituser = new Request_edit_user(user_id,nom.getText().toString(), mail.getText().toString(), mdp.getText().toString(), responseListener);
                RequestQueue queue = Volley.newRequestQueue(Activity_profile_change.this);
                queue.add(edituser);
            }
        });
    }
}
