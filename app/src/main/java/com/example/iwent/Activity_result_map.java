package com.example.iwent;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Activity_result_map extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private static final int TAG_CODE_PERMISSION_LOCATION = 123;
    SupportMapFragment mapFragment;
    GoogleMap map;
    ArrayList<HashMap<String, String>> elementsList;
    ArrayList<HashMap<String, String>> selectedList;
    ListView vue;
    Activity_result_map me = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_map);
        //ToolBar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView toolbarTitle = (TextView) findViewById(R.id.toolbarTitle);
        toolbarTitle.setText("Go home");
        LinearLayout layout = (LinearLayout) findViewById(R.id.LinearToolBar);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("ToolBar Clicked");
                Intent goHome = new Intent(Activity_result_map.this, Activity_home_preparation.class);
                Activity_result_map.this.startActivity(goHome);
            }
        });
        //boutton retour to list view
        final Button retourResultat = (Button) findViewById(R.id.retourResultat);
        retourResultat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultatIntent = new Intent(Activity_result_map.this, Activity_result_list.class);
                Activity_result_map.this.startActivity(resultatIntent);
            }
        });
        //map
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView2);
        mapFragment.getMapAsync(this);
        //recup liste de la vue liste
        elementsList = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("elementsList");
        System.out.println(" ---------- ELEMENTS LIST " + elementsList);
        //affichage vue
        vue = (ListView) findViewById(R.id.listView);
        vue.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("----------------- Position clické dans liste = " + position + "-------------------------");
                ChangeActivity(view, position);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        LatLng coordEvent;
        map.setOnMarkerClickListener(this);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        } else {
            ActivityCompat.requestPermissions(this, new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION },
                    TAG_CODE_PERMISSION_LOCATION);
        }

        boolean premierElement = true;
        for (HashMap<String, String> element : elementsList){
            coordEvent = new LatLng(Double.parseDouble(element.get("latitude")),Double.parseDouble(element.get("longitude")));
            googleMap.addMarker(new MarkerOptions().position(coordEvent).title(element.get("name")));
            if(premierElement)
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(coordEvent));
            premierElement=false;
        }

    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        LatLng position = marker.getPosition();
        Double latitude = position.latitude;
        Double longitude = position.longitude;
        List<HashMap<String, String>> selectedElementsList;
        selectedElementsList = new ArrayList<>();
        selectedList = new ArrayList<>();
        for (HashMap<String, String> element : elementsList){
            if(Double.parseDouble(element.get("latitude"))==latitude)
                if(Double.parseDouble(element.get("longitude"))==longitude) {
                    HashMap<String, String> selectedElement = new HashMap<String,String>();
                    for(Map.Entry<String, String> e : element.entrySet()) {
                        selectedElement.put(e.getKey(), e.getValue());
                        selectedList.add(element);
                    }
                    selectedElementsList.add(selectedElement);
                }
        }

        ListAdapter adapter = new SimpleAdapter(
                me,
                selectedElementsList,
                R.layout.row_result,
                new String[]{"name", "startDate", "address"},
                new int[]{R.id.name, R.id.startDate, R.id.address});
        vue.setAdapter(adapter);
        return false;
    }

    public void ensureSituation(final String eventId) {
        //récupération de l'userId
        String user_id = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getString("user_id", "");
        System.out.println("Mon User_id est " + user_id);
        //requête php à la BDD
        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsResponse = new JSONObject(response);
                    boolean absent = jsResponse.getBoolean("absent");
                    String situation;
                    if(absent) {
                        situation = "absent";
                    } else {
                        situation = "present";
                    }
                    SharedPreferences pref = getSharedPreferences("PREFERENCE", MODE_PRIVATE);
                    pref.edit().putString("situation", situation).apply();
                    String sit = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getString("situation", "");
                    String theSit = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getString("situation", "");
                    Intent intent = new Intent(Activity_result_map.this, Activity_event.class);
                    intent.putExtra("id", eventId);
                    startActivity(intent);
                } catch (JSONException e) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Activity_result_map.this);
                    builder.setMessage("Register Failed\nEmail already used").setNegativeButton("Retry", null).create().show();
                }
            }
        };
        Request_event_situation request = new Request_event_situation(eventId, user_id, listener);
        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);
    }

    public void ChangeActivity(View view, int position) {
        String id = selectedList.get(position).get("id");
        ensureSituation(id);
    }
}
