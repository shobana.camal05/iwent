package com.example.iwent;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class Request_register extends StringRequest {

    private static final String REGISTER_REQUEST_URL = "https://iwentcnam.000webhostapp.com/Register.php";
    private Map<String, String> params;

    public Request_register(String username, String mail, String password, Response.Listener<String> listener){
        super(Method.POST, REGISTER_REQUEST_URL, listener, null);
        params = new HashMap<>();
        params.put("username", username);
        params.put("mail", mail);
        params.put("password", password);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}
